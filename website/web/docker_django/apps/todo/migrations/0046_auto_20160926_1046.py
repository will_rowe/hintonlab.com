# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-26 09:46
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0045_auto_20160926_1045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='due_date',
            field=models.DateField(default=datetime.datetime(2016, 9, 26, 9, 46, 33, 899496, tzinfo=utc)),
        ),
    ]
