# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-26 09:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('databases', '0022_auto_20160926_1048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meetings',
            name='TIME',
            field=models.TimeField(default='10:52', verbose_name='TIME'),
        ),
    ]
