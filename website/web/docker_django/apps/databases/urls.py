from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^mutant-database/$', views.mutant_database, name='mutant-database'),
    url(r'^mutant-database/MUTANT-search/$', views.MUTANT_search, name='MUTANT-search'),
    url(r'^isolate-database/$', views.isolate_database, name='isolate-database'),
    url(r'^isolate-database/ISOLATE-search/$', views.ISOLATE_search, name='ISOLATE-search'),
]
